package log4dev

import (
	"fmt"
	"os"
	"strings"
	"time"

	color "github.com/TwiN/go-color"
)

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Logger
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
type Logger struct{}

func getTimestamp() string {
	return time.Now().Format("2006-01-02T15:04:05")
}

func output(logLevelString string, args ...interface{}) {
	var b strings.Builder

	// timestamp and log level
	b.WriteString(getTimestamp())
	b.WriteString(" ")
	b.WriteString(logLevelString)

	// convert user inputs into a string
	s := ""
	for _, arg := range args {
		s = fmt.Sprintf(" %v", arg)
		b.WriteString(s)
	}
	b.WriteString("\n")

	os.Stdout.WriteString(b.String())
}

func (Log Logger) Info(args ...interface{}) {
	logLevelString := color.Cyan + "Info" + color.Reset
	output(logLevelString, args...)
}

func (Log Logger) Debug(args ...interface{}) {
	logLevelString := color.Green + "Debug" + color.Reset
	output(logLevelString, args...)
}

// Log.Warning() and Log.Warn() are functionally equivalent
func (Log Logger) Warning(args ...interface{}) {
	logLevelString := color.Yellow + "Warn" + color.Reset
	output(logLevelString, args...)
}
func (Log Logger) Warn(args ...interface{}) {
	logLevelString := color.Yellow + "Warn" + color.Reset
	output(logLevelString, args...)
}

func (Log Logger) Error(args ...interface{}) {
	logLevelString := color.Red + "Error" + color.Reset
	output(logLevelString, args...)
}

func (Log Logger) Fatal(args ...interface{}) {
	logLevelString := color.Red + "Fatal" + color.Reset
	output(logLevelString, args...)
	os.Exit(1)
}
