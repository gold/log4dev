package log4dev

import (
	"io/ioutil"
	"os"
	"reflect"
	"regexp"
	"strings"
	"testing"
)

const (
	//                                 timestamp             level  log content
	//                                     |                    |    |
	LOG_PATTERN       = `^(\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d) (\S+) (.+)\n$`
	TIMESTAMP_PATTERN = `^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d$`
)

var (
	Log                = Logger{}
	logObj             = reflect.ValueOf(Log)
	logPatternRx       = regexp.MustCompile(LOG_PATTERN)
	timestampPatternRx = regexp.MustCompile(TIMESTAMP_PATTERN)
)

type logTest struct {
	logLevel    string
	description string
	input       interface{}
	expected    string
}

var logTests = []logTest{
	logTest{
		"Info",	"Log.Info() with string, array",
		[]any{"This is not a test. (Magritte)",	[...]string{"a", "b", "c"}},
		"[This is not a test. (Magritte) [a b c]]",
	},
	logTest{
		"Info",	"Log.Info() with string, bool",
		[]any{"NaN is not a number in JS:",	false},
		"[NaN is not a number in JS: false]",
	},
	logTest{
		"Debug", "Log.Debug() with int, int, bool, bool",
		[]any{"Variadic functions r helpful:", -33, 1337, true, false},
		"[Variadic functions r helpful: -33 1337 true false]",
	},
	logTest{
		"Debug", "Log.Debug() with nil, float",
		[]any{"Nothing to see here:", nil, 3.14},
		"[Nothing to see here: <nil> 3.14]",
	},
	logTest{
		"Warn", "Log.Warn() with string, bool",
		[]any{"NaN is not a number in JS:",	false},
		"[NaN is not a number in JS: false]",
	},
	logTest{
		"Warn", "Log.Warn() with jack",
		[]any{"Nothing ventured, nothing gained"},
		"[Nothing ventured, nothing gained]",
	},
	logTest{
		"Error", "Log.Error() with zilch",
		[]any{},
		"[]",
	},
	logTest{
		"Error", "Log.Error() with string, map, array, bool, int, float",
		[]any{"different stuff:", "wat?", map[string]string{"a": "alpha", "b": "beta"}, []bool{false, false}, false, 2497600, 1.1},
		"[different stuff: wat? map[a:alpha b:beta] [false false] false 2497600 1.1]",
	},
}

func TestLogMethods(t *testing.T) {

	for _, test := range logTests{
		t.Run(test.description, func(t *testing.T) {

			rescueStdout := os.Stdout
			r, w, _ := os.Pipe()
			os.Stdout = w

			in := make([]reflect.Value, 1)
			in[0] = reflect.ValueOf(test.input)

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			// Call the Log method: Info, Debug, Warn, Error, Fatal
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			logObj.MethodByName(test.logLevel).Call(in)

			w.Close()
			actual, _ := ioutil.ReadAll(r)
			os.Stdout = rescueStdout

			match := logPatternRx.FindStringSubmatch(string(actual))
			if len(match) == 0 {
				t.Errorf("got %q but expected %q", actual, test.expected)
				return
			}

			logLevel := match[2]
			if !strings.Contains(logLevel, test.logLevel) {
				t.Errorf("expected log level to be %q, but got %q", test.logLevel, logLevel)
			}

			logContent := match[3]
			if logContent != test.expected {
				t.Errorf("expected log content to be %q, but got %q", test.expected, logContent)
			}

		})
	}
}

func TestGetTimestamp(t *testing.T) {
	ts := getTimestamp()

	expectedFormat := timestampPatternRx.MatchString(ts)
	if !expectedFormat {
		t.Errorf("expected timestamp to be in ISO format, but got %q", ts)
	}
}
