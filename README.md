# log4dev

## Overview

`log4dev` is quick and easy to set up and use.

During my first experiences with Go development (creating REST API services and the like), I wanted a simple logger to:

* spew out various data objects to stdout for debugging purposes

* optionally use as a real logger at server run-time

It was designed for simplicty yet functional enough to satisfy my modest needs.

`log4dev` is not a full-featured logger with many configuration options; there are several Go packages already available to meet more ambitious requirements.

The intent is for `log4dev` to be fast and painless. I hope it helps your Go development efforts as much as it helps mine!

## Features

`log4dev` supports log levels Info, Debug, Warn, Error, and Fatal. Each log entry displays a color-coded log level string.

Each log entry is in the following format:

`<timestamp> <loglevel> <content derived from input args>`

Note: `Fatal` method will exit the program after spewing its log entry to stdout. This exit behavior is useful in special circumstances.

(The behavior of `log4dev` was partly inspired from JavaScript's variadic `console.info()` function.)

## Installation

```console
go get codeberg.org/gold/log4dev
```
## Usage and Examples

A couple of examples in main() illustrates basic usage.

```go
import codeberg.org/gold/log4dev

var Log = log4dev.Logger{}

func main() {
  port := 2525
  Log.Info("Starting server on port", port)

  // 2022-08-27T15:36:00 Info Starting server on port 2525

  isValid := true
  directions := []string{"up", "down", "northeast"}
  Log.Debug("isValid & directions:", isValid, directions)

  // 2022-08-27T15:36:00 Debug isValid & directions: true [up down northeast]
}
```

As you can see, a variable number of arguments of arbitrary data types can be used in any of the Log methods. The output will separate each argument's value with a space, all on a single line.

## Credits

Gerry Gold

August 2022

Have fun!
